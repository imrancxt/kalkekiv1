<?php

namespace App\Http\Controllers;

use DB;

class customize extends Controller {

    public function index() {
        //
    }

    public function get_data($query, $var, $col) {
       // echo"test";
        try {
            $info = array();
            //echo $query;
            $result = DB::select($query);
            
            if (count($result) > 0) {
                $i = 0;
                foreach ($result as $row) {
                    for ($j = 0; $j < count($var); $j++) {
                        $info[$var[$j]][$i]=$row->$col[$j];
                    }
                    $i++;
                }
            }
            //print_r($info);
            return $info;
        } catch (\Illuminate\Database\QueryException $e) {
            return FALSE;
        }
    }

}
