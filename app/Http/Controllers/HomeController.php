<?php

namespace App\Http\Controllers;

//use Blade;
//require app_path()."/includes/common.php";
class HomeController extends Controller {

    private $cust;

    public function __construct() {
        $this->cust = new customize();
    }

    public function getIndex() {
        $query = "SELECT * FROM `movie_time`";
        $data = $this->cust->get_data($query, array('movie_name','date','time'), array('movie_name','date','time'));
        return view('home', $data);
    }
    public function weather(){
        return view('weather');
    }
    public function getTestajax(){
        echo"hi...";
    }

}
