@extends('app')
@section('page-heading')
All events
@endsection
@section('content')
<div class="row">
    <div class="col s8">
        <!--<button class='link' page='/admin/testajax'>Test</button>-->
        <table class="bordered">
            <thead>
                <tr>
                    <th data-field="id">Movie Name</th>
                    <th data-field="name">Date</th>
                    <th data-field="price">Time</th>
                    <th data-field="price">Details</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (isset($movie_name)) {
                    $i = 0;
                    foreach ($movie_name as $movie) {
                        echo"<tr><td>$movie</td><td>$date[$i]</td><td>$time[$i]</td><td><a class='waves-effect waves-light btn' ng-click=\"getdata('$movie')\">Details</a></td></tr>";
                        $i++;
                    }
                }
                ?>
            </tbody>
        </table>

    </div>
    <div class="col s4">
        <!--<div class="card">
            <div class="card-image">
                <img src="{{info.Poster}}">
                <span class="card-title">{{info.Title}}</span>
            </div>
            <div class="card-content">
                <p>{{info.Plot}}</p>
            </div>
            <div class="card-action">
                <a href="#">This is a link</a>
            </div>
        </div>
    </div>-->
        <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
                <img class="activator" width="250" height="300" src="{{info.Poster}}">
            </div>
            <div class="card-content">
                <span class="card-title activator grey-text text-darken-4">{{info.Title}}<i class="material-icons right">more_vert</i></span>
                <p>Release Date:{{info.Released}}<br>Rating:{{info.imdbRating}}&nbsp;&nbsp;Votes:{{info.imdbVotes}}<br>
                    Director:{{info.Director}}<br>
                    Run Time:{{info.Runtime}}<br>
                    Movie Type:{{info.Genre}}<br>
                    Language:{{info.Language}}<br>
                    Response:{{info.Response}}<br>
                    Rated:{{info.Rated}}<br>
                </p>
            </div>
            <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Full Information<i class="material-icons right">close</i></span>
                <p>Details<hr>{{info.Plot}}</p>
                <p>Casts<hr>{{info.Actors}}</p>
                <p>Writers<hr>{{info.Writer}}</p>
                <p>Awards<hr>{{info.Awards}}</p>
            </div>
        </div>

    </div>

    <script>
        var app = angular.module("myApp", []);
        app.controller("myCtrl", function($scope,$http) {
            $scope.getdata=function(movie_name){
                url="http://www.omdbapi.com/?t="+movie_name+"&y=&plot=full&r=json";
                $http.get(url)
                .then(function (response) {$scope.info = response.data;});
            }
            $scope.getdata("Titanic");
        });
    </script>
    @endsection
