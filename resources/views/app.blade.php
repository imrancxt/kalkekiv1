<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
        <title>Kalke Ki?</title>

        <!-- CSS  -->
        <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="css/styles.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="js/materialize.js"></script>
        <script src="js/application.js"></script>
        <script src="js/customize.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    </head>
    <body>



        <header>    
            <nav class="top-nav">
                <div class="container">
                    <div class="nav-wrapper">
                        <a class="page-title">@yield('page-heading')</a>
                        @if(Auth::check())
                        <form action="/logout" method="POST" style="float: right;"><input type="submit" value="Log out"></form>
                        @endif
                    </div>
                </div>
            </nav>

            <div class="container"><a href="#" data-activates="nav-mobile" class="button-collapse top-nav full"><i class="mdi-navigation-menu"></i></a></div>

            <ul id="nav-mobile" class="side-nav fixed">
                <li class="logo"><a id="logo-container" href="/" class="brand-logo"><span class="logo-red">Kalke</span><span class="logo-white">Ki?</span></a></li>
                <li class="waves-effect waves-teal"><a href="/"><i class="mdi-maps-terrain"></i> Explore All</a></li>
                <li class="waves-effect waves-teal"><a href="../public/weather"><i class="mdi-maps-local-bar"></i>Weather</a></li>
                <li class="waves-effect waves-teal"><a href="../public/admin"><i class="mdi-social-public"></i>Movie Event</a></li>
                <li class="waves-effect waves-teal"><a href="/education"><i class="mdi-social-school"></i> Education</a></li>
                <li class="waves-effect waves-teal"><a href="/meetings"><i class="mdi-social-people"></i> Meetings</a></li>
                <li class="waves-effect waves-teal"><a href="/music-arts"><i class="mdi-hardware-headset"></i> Music/Arts</a></li>
                <li class="waves-effect waves-teal"><a href="/sports"><i class="mdi-av-games"></i> Sports</a></li>
                <li class="waves-effect waves-teal"><a href="/trips"><i class="mdi-maps-map"></i> Trips</a></li>
                <li class="waves-effect waves-teal"><a href="/others"><i class="mdi-action-dashboard"></i> Others</a></li>
            </ul>
        </header>





    <main>
        <div class="container" ng-app="myApp" ng-controller="myCtrl" id='main_container'>
            @yield('content')
        </div>
    </main>



    <!--  Scripts-->

</body>
</html>

