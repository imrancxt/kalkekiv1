
$(".button-collapse").sideNav();

$(document).ready(function(){
    var url="event";
      $(".approve-event").click(function(){
      	var btn = $(this);
      	btn.html('working...');
        var val = $(this).parents('tr').find('.json-format').html();
        var val2 = $(this).parents('tr').find('select[name="event_types"]').val();
        $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json',
            data: JSON.stringify({
            	'event': JSON.parse(val),
            	'categories': val2
            }),
            success: function(data) {
            	console.log(data);
            	btn.html('done!');
            	btn.closest('tr').fadeOut('slow');
            }
        });
    });

    $(".reject-event").click(function(){
      	var btn = $(this);
      	btn.html('working...');
        var val = $(this).parents('tr').find('.json-format').html();
        $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json',
            data: JSON.stringify({
            	'event': JSON.parse(val),
            	'reject': true
            }),
            success: function(data) {
            	console.log(data);
            	btn.html('done!');
            	btn.closest('tr').fadeOut('slow');
            }
        });
    });
});
